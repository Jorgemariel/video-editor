import { getJSON, setJSON } from 'helpers/localStoraje';
import _ from 'lodash';

export const getProjects = () => {
  return getJSON('projects');
}

export const getProject = id => {
  const list = getProjects();
  return _.find(list, { id });
}

export const saveProject = project => {
  const data = [
    ...getProjects(),
    project
  ]
  setJSON('projects', data);
}

export const editProject = project => {
  const list = getProjects();
  const idx = _.findIndex(list, { 'id': project.id });

  list[idx] = project
  setJSON('projects', list);
}

export const deleteProject = id => {
  let list = getProjects();
  const idx = _.findIndex(list, { id });
  list.splice(idx, 1);
  setJSON('projects', list);
}