export const getJSON = key => {
  return JSON.parse(localStorage.getItem(key)) || [];
}

export const setJSON = (key, data) => {
  localStorage.setItem(key, JSON.stringify(data));
}