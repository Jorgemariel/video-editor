// Base
import React from 'react';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import uuidv1 from 'uuid/v1';
import _ from 'lodash';
import moment from 'moment';
import keydown from 'react-keydown';
import { withStyles } from '@material-ui/core/styles';
import { Collapse } from 'react-collapse';

// Material-UI
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';

// Icons
import CloseIcon from '@material-ui/icons/Close';

// Custom
import Title from 'components/title';
import VideoList from 'components/list';
import Actions from 'components/actions';
import Clip from 'components/clip';
import SourceDialog from 'components/sourceDialog';
import ResetDialog from 'components/resetDialog';
import InfoDialog from 'components/infoDialog';
import DeleteDialog from 'components/deleteDialog';
import SaveProjectDialog from 'components/saveProjectDialog';
import NoVideo from 'components/noVideo';

// Actions
import { saveProject, getProjects, editProject, deleteProject } from 'actions/project'
import Drawer from 'components/drawer';


const Player = dynamic(import('components/player'), {
  ssr: false,
  loading: () => <NoVideo loading={true} onStopTimer={() => (true)} openSourceDialog={() => (true)} />,
});

const styles = theme => ({
  root: {
    textAlign: 'center',
    paddingTop: theme.spacing.unit * 5,
    maxWidth: 600,
    margin: '0 auto',
    marginBottom: 80,
    padding: '0 15px',
  },
  about: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
    textTransform: 'none',
    fontSize: '1.5em'
  },
});

// Global var for timeout
let timer;

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.player = React.createRef();
  }

  componentDidMount() {
    this.setState({ projects: getProjects() })
  }

  state = {
    id: null,
    name: null,
    source: null,
    selected: null,
    formErrors: null,
    loading: false,
    play: false,
    edit: false,
    clips: [],
    sourceDialog: false,
    resetDialog: false,
    infoDialog: false,
    deleteDialog: false,
    saveProjectDialog: false,

    snackbar: null,
    drawer: false,
  };

  getSrc = () => {
    let { source, selected } = this.state;

    return source ? `${source}#t=${selected && selected.start || ''},${selected && selected.end}` : '';
  }

  setSourceDialog = sourceDialog => {
    this.setState({ sourceDialog });
  };

  setSource = () => {
    const { sourceDialog } = this.state;
    if (sourceDialog === '' || sourceDialog === true) return this.setSnackbar('The source link is required.');

    this.setState({
      source: sourceDialog,
      sourceDialog: false,
    });


    setTimeout(() => {
      const { source } = this.state;

      if (source) {
        this.setSnackbar('The video was correctly upload.');
      }
    }, 200);
  };

  setSelected = async selected => {
    if (selected === 'source' || selected === false) {
      this.setState({ selected: false, edit: false });
    }
    else if (selected === true) {
      this.setState({ edit: true, selected: { name: 'New Clip', start: 0, end: false } });
    } else {
      this.setState({
        loading: true,
        selected: {
          ...selected,
          start: this.secondsToTime(selected.start),
          end: this.secondsToTime(selected.end),
        }
      });

      // Fake loading (1sec)
      await new Promise(resolve => setTimeout(() => {
        this.setState({
          loading: false,
        })
        resolve();
      }, 1000))
    }
  }

  setInfoDialog = infoDialog => {
    this.setState({ infoDialog });
  }

  setDeleteDialog = deleteDialog => {
    this.setState({ deleteDialog });
  }

  setSaveProjectDialog = saveProjectDialog => {
    if (saveProjectDialog === true) {
      const { name } = this.state;
      saveProjectDialog = name || true;
    }
    this.setState({ saveProjectDialog });
  }

  setDrawer = drawer => {
    this.setState({ drawer });
  }

  handleClipChange = (key, value) => {
    let { selected } = this.state;
    selected = {
      ...selected,
      [key]: value,
    }
    this.setState({ selected });
    this.checkForm(selected);
  }

  checkForm = ({ name, start, end }) => {
    const errors = {
      name: this.nameHasError(name),
      start: this.timeHasError(start),
      end: this.timeHasError(end),
    }

    if (errors.name || errors.start || errors.end) {
      this.setState({ formErrors: errors });
      return false;
    }

    if (start && end && this.timeToSeconds(end) - this.timeToSeconds(start) <= 0) {
      this.setState({ formErrors: { end: 'The end time must be greater than the start time' } });
      return false;
    }

    if (start && this.timeToSeconds(start) > this.player.duration()) {
      this.setState({ formErrors: { start: 'Can not be greater than the video length' } });
      return false;
    }

    if (end && this.timeToSeconds(end) > this.player.duration()) {
      this.setState({ formErrors: { end: 'Can not be greater than the video length' } });
      return false;
    }

    this.setState({ formErrors: null });
    return true;
  }

  nameHasError = name => {
    if (name.length === 0) return 'The name is required';
    if (name.length > 100) return 'The name is to long';

    return false
  }

  timeHasError = time => {
    if (!time) return false;
    if (!isNaN(time) && Number(time) >= 0) return false;
    if (moment(time, 'H:m:s', true).isValid()) return false;
    if (moment(time, 'm:s', true).isValid()) return false;

    return 'The time format is not valid. Valid format: 00:00:00 or 00:00';
  }

  setCurrentTime = key => {
    this.handleClipChange(key, this.secondsToTime(this.player.time()));
  }

  timeToSeconds = time => {
    if (!time) return 0;
    if (!isNaN(time) && Number(time) >= 0) return time;
    if (moment(time, 'H:m:s', true).isValid()) return moment(time, 'H:m:s').diff(moment().startOf('day'), 'seconds');
    if (moment(time, 'm:s', true).isValid()) return moment(time, 'm:s').diff(moment().startOf('day'), 'seconds');
  }

  secondsToTime = time => {
    return moment.utc(time * 1000).format('HH:mm:ss');
  }

  saveClip = () => {
    let { selected, clips } = this.state;

    if (selected.id) {
      //Edit
      const idx = _.findIndex(clips, { 'id': selected.id });
      clips[idx] = {
        ...selected,
        start: this.timeToSeconds(selected.start),
        end: this.timeToSeconds(selected.end),
      };

      this.setState({ clips, selected: false });

      this.setSnackbar('The clip was successfully edited.');
    } else {
      //Create
      selected = {
        ...selected,
        id: uuidv1(),
        start: this.timeToSeconds(selected.start),
        end: this.timeToSeconds(selected.end),
      };

      clips = [
        ...clips,
        selected,
      ];

      this.setState({ clips, selected: false, edit: false, });

      this.setSnackbar('The clip was successfully saved.');
    }
  }

  editClip = id => {
    const { clips } = this.state;
    const selected = _.keyBy(clips, 'id')[id];

    this.setState({ edit: true });

    this.setSelected(selected);
  }

  onDeleteClip = clip => {
    let { clips, selected } = this.state;
    const idx = clips.indexOf(clip);
    clips.splice(idx, 1);

    if (selected && selected.id === clip.id) {
      this.setState({ clips, deleteDialog: false, selected: false });
    } else {
      this.setState({ clips, deleteDialog: false });
    }

    this.setSnackbar('The clip was successfully deleted.');

  }

  togglePlay = () => {
    this.setState({ play: !this.state.play })
  }

  setPlay = play => {
    this.setState({ play })
  }

  onPlay = async data => {
    try {
      if (data !== 'preview') {
        await this.setSelected(data);
        this.setState({ edit: false })
      }

      this.player.load();
      this.player.play();
    } catch (error) {
      console.log({ error })
    }
  }

  toggleResetDialog = () => {
    this.setState({ resetDialog: !this.state.resetDialog });
  }

  onReset = error => {
    this.setState({
      source: null,
      selected: null,
      edit: false,
      play: false,
      clips: [],
      resetDialog: false,
    });

    if (error !== true) {
      this.setSnackbar('The project was successfully reset.');
    }
  }

  onError = e => {
    this.setSnackbar('Something went wrong. Try again.');
    this.onReset(true);
  }

  onPause = async () => {
    const { selected } = this.state;
    this.setPlay(false);

    if (selected && this.timeToSeconds(selected.end) === Math.round(this.player.time())) {
      const next = this.getNextVideo();

      if (next) {
        await this.setLoadingTimer();
        this.onPlay(next)
      }
    }
  }

  setLoadingTimer = async () => {
    await new Promise(resolve => timer = setTimeout(() => {
      this.setState({
        loading: '3',
      })
      resolve();
    }, 1000));

    await new Promise(resolve => timer = setTimeout(() => {
      this.setState({
        loading: '2',
      })
      resolve();
    }, 1000));

    await new Promise(resolve => timer = setTimeout(() => {
      this.setState({
        loading: '1',
      })
      resolve();
    }, 1000));

    await new Promise(resolve => timer = setTimeout(() => {
      this.setState({
        loading: false,
      })
      resolve();
    }, 1000));
  }

  onStopTimer = () => {
    clearTimeout(timer);
    this.setState({ loading: false })
  }

  setSnackbar = snackbar => {
    this.setState({ snackbar })
  }

  getNextVideo = () => {
    const { selected, edit, clips, source } = this.state;

    //if source video
    if (!selected && !edit && source) {
      console.log('es source')
      if (clips[0]) return clips[0];
      else return null
    }

    if (edit) return null;
    if (selected && !selected.id) return null;

    const idx = _.findIndex(clips, { 'id': selected.id });
    if (!clips[idx + 1]) return null

    return clips[idx + 1];
  }

  getPreviousVideo = () => {
    const { selected, edit, clips } = this.state;

    //first clip
    if (selected && selected.id === clips[0].id) return 'source';


    if (edit) return null;
    if (selected && !selected.id) return null;

    const idx = _.findIndex(clips, { 'id': selected.id });
    if (!clips[idx - 1]) return null

    return clips[idx - 1];
  }

  @keydown('enter')
  async onEnterKeyPress() {
    try {
      const { play, source } = this.state;

      if (source && play) this.player.pause();;
      if (source && !play) this.player.play();
    } catch (error) {
      console.log({ error })
    }
  }

  @keydown(37) // ArrowLeft
  async onArrowLeftKeyPress() {
    const { selected } = this.state;
    const prev = selected && selected.id ? await this.getPreviousVideo() : null;

    if (prev) this.onPlay(prev);
    else this.setSnackbar('No previous video');
  }

  @keydown(39) // ArrowRight
  async onArrowRightKeyPress() {
    const next = await this.getNextVideo() || null;

    if (next && next === 'source') this.onPlay(null);
    if (next) this.onPlay(next);
    else this.setSnackbar('The are no more videos to play');
  }

  onSaveProject = () => {
    const { id, saveProjectDialog, source, clips } = this.state;

    if (id) {
      // Edit
      const data = {
        id,
        source,
        clips,
        name: saveProjectDialog,
      };
      editProject(data);
      this.setSnackbar('The project was successfully edited.');
    } else {
      // Create
      const data = {
        id: uuidv1(),
        source,
        clips,
        name: saveProjectDialog,
      };
      saveProject(data);
      this.setSnackbar('The project was successfully saved.');

    }

    this.setState({ projects: getProjects(), saveProjectDialog: false });
  }

  onOpenProject = ({ id, source, clips, name }) => {
    this.setState({ id, source, clips, name });
    this.setSnackbar('The project was successfully opened.');
  }

  onDeleteProject = id => {
    deleteProject(id);
    this.setState({ projects: getProjects() });
    this.setSnackbar('The project was successfully deleted.');
  }

  render() {
    const { classes } = this.props;
    const {
      id,
      name,
      source,
      selected,
      formErrors,
      loading,
      edit,
      clips,
      sourceDialog,
      resetDialog,
      infoDialog,
      deleteDialog,
      saveProjectDialog,
      snackbar,
      drawer,
      projects,
    } = this.state;

    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <Title title='Video editor' subtitle='By Jorge Mariel' name={name} />
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              {!!source && !loading &&
                <Player
                  src={this.getSrc()}
                  start={selected ? selected.start : '10'}
                  end={selected ? selected.end : false}
                  setPlay={this.setPlay}
                  onPause={this.onPause}
                  onError={this.onError}
                  onRef={ref => (this.player = ref)}
                />
              }
              {(!source || loading) &&
                <NoVideo
                  loading={loading}
                  onStopTimer={this.onStopTimer}
                  openSourceDialog={() => this.setSourceDialog(true)}
                  openDrawer={() => this.setDrawer(true)}
                />
              }
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Collapse isOpened={!!selected} style={{ overflow: 'inherit' }}>
              {!!selected && !!edit &&
                <Clip
                  data={selected}
                  errors={formErrors}
                  handleChange={this.handleClipChange}
                  setCurrentTime={this.setCurrentTime}
                  onClose={() => this.setSelected(false)}
                  onPreview={this.onPlay}
                  onSave={this.saveClip}
                />
              }
            </Collapse>
            <Collapse isOpened={!edit && !!source} style={{ overflow: 'inherit' }}>
              {!edit && !!source &&
                <Actions
                  onNewClip={() => this.setSelected(true)}
                />
              }
            </Collapse>
          </Grid>
          <Grid item xs={12}>
            <Collapse isOpened={!!source} style={{ overflow: 'inherit' }}>
              {!!source &&
                <Paper className={classes.paper}>
                  <VideoList
                    source={source}
                    items={clips}
                    selected={selected}
                    timeFormat={this.secondsToTime}
                    onPlay={this.onPlay}
                    onEdit={this.editClip}
                    onDelete={this.setDeleteDialog}
                    onReset={this.toggleResetDialog}
                  />
                </Paper>
              }
            </Collapse>
          </Grid>
          <Grid item xs={12}>
            <Collapse isOpened={!!source} style={{ overflow: 'inherit' }}>
              {!!source &&
                <Button variant="contained" color="primary" component="span" className={classes.button} onClick={() => this.setSaveProjectDialog(true)}>
                  {id ? 'Edit Project' : 'Save Project'}
                </Button>
              }
            </Collapse>
          </Grid>
        </Grid>
        <Fab
          onClick={() => this.setInfoDialog(true)}
          className={classes.about}
          color="primary"
          aria-label="About"
        >
          i
        </Fab>


        {/* DIALOGS */}
        <SourceDialog
          value={sourceDialog}
          closeDialog={() => this.setSourceDialog(false)}
          handleSourceChange={this.setSourceDialog}
          onSubmit={this.setSource}
        />
        <ResetDialog
          open={resetDialog}
          onOk={this.onReset}
          onCancel={this.toggleResetDialog}
        />
        <InfoDialog
          open={infoDialog}
          onClose={() => this.setInfoDialog(false)}
        />
        <DeleteDialog
          value={deleteDialog}
          onOk={this.onDeleteClip}
          onCancel={() => this.setDeleteDialog(false)}
        />
        <SaveProjectDialog
          value={saveProjectDialog}
          closeDialog={() => this.setSaveProjectDialog(false)}
          handleNameChange={this.setSaveProjectDialog}
          onSubmit={this.onSaveProject}
        />

        <Drawer
          open={drawer}
          projects={projects}
          onClose={() => this.setDrawer(false)}
          onOpenProject={this.onOpenProject}
          onDeleteProject={this.onDeleteProject}
        />

        {/* SNACKBAR */}
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={!!snackbar}
          autoHideDuration={6000}
          onClose={() => this.setSnackbar(false)}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{snackbar}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={() => this.setSnackbar(false)}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />
      </div>
    );
  }
}

Index.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Index);
