webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/next/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/styles/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var uuid_v1__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! uuid/v1 */ "./node_modules/uuid/v1.js");
/* harmony import */ var uuid_v1__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(uuid_v1__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_collapse__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-collapse */ "./node_modules/react-collapse/lib/index.js");
/* harmony import */ var react_collapse__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_collapse__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_keydown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-keydown */ "./node_modules/react-keydown/es/index.js");
/* harmony import */ var _material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/Paper */ "./node_modules/@material-ui/core/Paper/index.js");
/* harmony import */ var _material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/Grid/index.js");
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _material_ui_core_Fab__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/core/Fab */ "./node_modules/@material-ui/core/Fab/index.js");
/* harmony import */ var _material_ui_core_Fab__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Fab__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/core/Snackbar */ "./node_modules/@material-ui/core/Snackbar/index.js");
/* harmony import */ var _material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @material-ui/core/IconButton */ "./node_modules/@material-ui/core/IconButton/index.js");
/* harmony import */ var _material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @material-ui/icons/Close */ "./node_modules/@material-ui/icons/Close.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _components_title__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../components/title */ "./components/title.js");
/* harmony import */ var _components_list__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../components/list */ "./components/list.js");
/* harmony import */ var _components_actions__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../components/actions */ "./components/actions.js");
/* harmony import */ var _components_clip__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../components/clip */ "./components/clip.js");
/* harmony import */ var _components_sourceDialog__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../components/sourceDialog */ "./components/sourceDialog.js");
/* harmony import */ var _components_resetDialog__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../components/resetDialog */ "./components/resetDialog.js");
/* harmony import */ var _components_infoDialog__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../components/infoDialog */ "./components/infoDialog.js");
/* harmony import */ var _components_deleteDialog__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../components/deleteDialog */ "./components/deleteDialog.js");
/* harmony import */ var _components_noVideo__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../components/noVideo */ "./components/noVideo.js");

var _jsxFileName = "/home/jorge/Dev/Jobsity/Coding Challenge/video-editor/pages/index.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _decorate(decorators, factory, superClass) { var r = factory(function initialize(O) { _initializeInstanceElements(O, decorated.elements); }, superClass); var decorated = _decorateClass(_coalesceClassElements(r.d.map(_createElementDescriptor)), decorators); _initializeClassElements(r.F, decorated.elements); return _runClassFinishers(r.F, decorated.finishers); }

function _createElementDescriptor(def) { var key = _toPropertyKey(def.key); var descriptor; if (def.kind === "method") { descriptor = { value: def.value, writable: true, configurable: true, enumerable: false }; Object.defineProperty(def.value, "name", { value: _typeof(key) === "symbol" ? "" : key, configurable: true }); } else if (def.kind === "get") { descriptor = { get: def.value, configurable: true, enumerable: false }; } else if (def.kind === "set") { descriptor = { set: def.value, configurable: true, enumerable: false }; } else if (def.kind === "field") { descriptor = { configurable: true, writable: true, enumerable: true }; } var element = { kind: def.kind === "field" ? "field" : "method", key: key, placement: def.static ? "static" : def.kind === "field" ? "own" : "prototype", descriptor: descriptor }; if (def.decorators) element.decorators = def.decorators; if (def.kind === "field") element.initializer = def.value; return element; }

function _coalesceGetterSetter(element, other) { if (element.descriptor.get !== undefined) { other.descriptor.get = element.descriptor.get; } else { other.descriptor.set = element.descriptor.set; } }

function _coalesceClassElements(elements) { var newElements = []; var isSameElement = function isSameElement(other) { return other.kind === "method" && other.key === element.key && other.placement === element.placement; }; for (var i = 0; i < elements.length; i++) { var element = elements[i]; var other; if (element.kind === "method" && (other = newElements.find(isSameElement))) { if (_isDataDescriptor(element.descriptor) || _isDataDescriptor(other.descriptor)) { if (_hasDecorators(element) || _hasDecorators(other)) { throw new ReferenceError("Duplicated methods (" + element.key + ") can't be decorated."); } other.descriptor = element.descriptor; } else { if (_hasDecorators(element)) { if (_hasDecorators(other)) { throw new ReferenceError("Decorators can't be placed on different accessors with for " + "the same property (" + element.key + ")."); } other.decorators = element.decorators; } _coalesceGetterSetter(element, other); } } else { newElements.push(element); } } return newElements; }

function _hasDecorators(element) { return element.decorators && element.decorators.length; }

function _isDataDescriptor(desc) { return desc !== undefined && !(desc.value === undefined && desc.writable === undefined); }

function _initializeClassElements(F, elements) { var proto = F.prototype; ["method", "field"].forEach(function (kind) { elements.forEach(function (element) { var placement = element.placement; if (element.kind === kind && (placement === "static" || placement === "prototype")) { var receiver = placement === "static" ? F : proto; _defineClassElement(receiver, element); } }); }); }

function _initializeInstanceElements(O, elements) { ["method", "field"].forEach(function (kind) { elements.forEach(function (element) { if (element.kind === kind && element.placement === "own") { _defineClassElement(O, element); } }); }); }

function _defineClassElement(receiver, element) { var descriptor = element.descriptor; if (element.kind === "field") { var initializer = element.initializer; descriptor = { enumerable: descriptor.enumerable, writable: descriptor.writable, configurable: descriptor.configurable, value: initializer === void 0 ? void 0 : initializer.call(receiver) }; } Object.defineProperty(receiver, element.key, descriptor); }

function _decorateClass(elements, decorators) { var newElements = []; var finishers = []; var placements = { static: [], prototype: [], own: [] }; elements.forEach(function (element) { _addElementPlacement(element, placements); }); elements.forEach(function (element) { if (!_hasDecorators(element)) return newElements.push(element); var elementFinishersExtras = _decorateElement(element, placements); newElements.push(elementFinishersExtras.element); newElements.push.apply(newElements, elementFinishersExtras.extras); finishers.push.apply(finishers, elementFinishersExtras.finishers); }); if (!decorators) { return { elements: newElements, finishers: finishers }; } var result = _decorateConstructor(newElements, decorators); finishers.push.apply(finishers, result.finishers); result.finishers = finishers; return result; }

function _addElementPlacement(element, placements, silent) { var keys = placements[element.placement]; if (!silent && keys.indexOf(element.key) !== -1) { throw new TypeError("Duplicated element (" + element.key + ")"); } keys.push(element.key); }

function _decorateElement(element, placements) { var extras = []; var finishers = []; for (var decorators = element.decorators, i = decorators.length - 1; i >= 0; i--) { var keys = placements[element.placement]; keys.splice(keys.indexOf(element.key), 1); var elementObject = _fromElementDescriptor(element); var elementFinisherExtras = _toElementFinisherExtras((0, decorators[i])(elementObject) || elementObject); element = elementFinisherExtras.element; _addElementPlacement(element, placements); if (elementFinisherExtras.finisher) { finishers.push(elementFinisherExtras.finisher); } var newExtras = elementFinisherExtras.extras; if (newExtras) { for (var j = 0; j < newExtras.length; j++) { _addElementPlacement(newExtras[j], placements); } extras.push.apply(extras, newExtras); } } return { element: element, finishers: finishers, extras: extras }; }

function _decorateConstructor(elements, decorators) { var finishers = []; for (var i = decorators.length - 1; i >= 0; i--) { var obj = _fromClassDescriptor(elements); var elementsAndFinisher = _toClassDescriptor((0, decorators[i])(obj) || obj); if (elementsAndFinisher.finisher !== undefined) { finishers.push(elementsAndFinisher.finisher); } if (elementsAndFinisher.elements !== undefined) { elements = elementsAndFinisher.elements; for (var j = 0; j < elements.length - 1; j++) { for (var k = j + 1; k < elements.length; k++) { if (elements[j].key === elements[k].key && elements[j].placement === elements[k].placement) { throw new TypeError("Duplicated element (" + elements[j].key + ")"); } } } } } return { elements: elements, finishers: finishers }; }

function _fromElementDescriptor(element) { var obj = { kind: element.kind, key: element.key, placement: element.placement, descriptor: element.descriptor }; var desc = { value: "Descriptor", configurable: true }; Object.defineProperty(obj, Symbol.toStringTag, desc); if (element.kind === "field") obj.initializer = element.initializer; return obj; }

function _toElementDescriptors(elementObjects) { if (elementObjects === undefined) return; return _toArray(elementObjects).map(function (elementObject) { var element = _toElementDescriptor(elementObject); _disallowProperty(elementObject, "finisher", "An element descriptor"); _disallowProperty(elementObject, "extras", "An element descriptor"); return element; }); }

function _toElementDescriptor(elementObject) { var kind = String(elementObject.kind); if (kind !== "method" && kind !== "field") { throw new TypeError('An element descriptor\'s .kind property must be either "method" or' + ' "field", but a decorator created an element descriptor with' + ' .kind "' + kind + '"'); } var key = _toPropertyKey(elementObject.key); var placement = String(elementObject.placement); if (placement !== "static" && placement !== "prototype" && placement !== "own") { throw new TypeError('An element descriptor\'s .placement property must be one of "static",' + ' "prototype" or "own", but a decorator created an element descriptor' + ' with .placement "' + placement + '"'); } var descriptor = elementObject.descriptor; _disallowProperty(elementObject, "elements", "An element descriptor"); var element = { kind: kind, key: key, placement: placement, descriptor: Object.assign({}, descriptor) }; if (kind !== "field") { _disallowProperty(elementObject, "initializer", "A method descriptor"); } else { _disallowProperty(descriptor, "get", "The property descriptor of a field descriptor"); _disallowProperty(descriptor, "set", "The property descriptor of a field descriptor"); _disallowProperty(descriptor, "value", "The property descriptor of a field descriptor"); element.initializer = elementObject.initializer; } return element; }

function _toElementFinisherExtras(elementObject) { var element = _toElementDescriptor(elementObject); var finisher = _optionalCallableProperty(elementObject, "finisher"); var extras = _toElementDescriptors(elementObject.extras); return { element: element, finisher: finisher, extras: extras }; }

function _fromClassDescriptor(elements) { var obj = { kind: "class", elements: elements.map(_fromElementDescriptor) }; var desc = { value: "Descriptor", configurable: true }; Object.defineProperty(obj, Symbol.toStringTag, desc); return obj; }

function _toClassDescriptor(obj) { var kind = String(obj.kind); if (kind !== "class") { throw new TypeError('A class descriptor\'s .kind property must be "class", but a decorator' + ' created a class descriptor with .kind "' + kind + '"'); } _disallowProperty(obj, "key", "A class descriptor"); _disallowProperty(obj, "placement", "A class descriptor"); _disallowProperty(obj, "descriptor", "A class descriptor"); _disallowProperty(obj, "initializer", "A class descriptor"); _disallowProperty(obj, "extras", "A class descriptor"); var finisher = _optionalCallableProperty(obj, "finisher"); var elements = _toElementDescriptors(obj.elements); return { elements: elements, finisher: finisher }; }

function _disallowProperty(obj, name, objectType) { if (obj[name] !== undefined) { throw new TypeError(objectType + " can't have a ." + name + " property."); } }

function _optionalCallableProperty(obj, name) { var value = obj[name]; if (value !== undefined && typeof value !== "function") { throw new TypeError("Expected '" + name + "' to be a function"); } return value; }

function _runClassFinishers(constructor, finishers) { for (var i = 0; i < finishers.length; i++) { var newConstructor = (0, finishers[i])(constructor); if (newConstructor !== undefined) { if (typeof newConstructor !== "function") { throw new TypeError("Finishers must return a constructor."); } constructor = newConstructor; } } return constructor; }

function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }

function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }

function _toArray(arr) { return _arrayWithHoles(arr) || _iterableToArray(arr) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

// Base








 // Material-UI





 // Icons

 // Custom










var Player = next_dynamic__WEBPACK_IMPORTED_MODULE_4___default()(function () {
  return Promise.all(/*! import() */[__webpack_require__.e("styles"), __webpack_require__.e(1)]).then(__webpack_require__.bind(null, /*! ../components/player */ "./components/player.js"));
}, {
  ssr: false,
  loading: function loading() {
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_noVideo__WEBPACK_IMPORTED_MODULE_24__["default"], {
      loading: true,
      onStopTimer: function onStopTimer() {
        return true;
      },
      openSourceDialog: function openSourceDialog() {
        return true;
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    });
  },
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(/*! ../components/player */ "./components/player.js")];
    },
    modules: ["../components/player"]
  }
});

var styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      paddingTop: theme.spacing.unit * 5,
      maxWidth: 600,
      margin: '0 auto',
      marginBottom: 80,
      padding: '0 15px'
    },
    about: {
      position: 'fixed',
      bottom: theme.spacing.unit * 2,
      right: theme.spacing.unit * 2,
      textTransform: 'none',
      fontSize: '1.5em'
    }
  };
}; // Global var for timeout


var timer;

var Index = _decorate(null, function (_initialize, _React$Component) {
  var Index =
  /*#__PURE__*/
  function (_React$Component2) {
    _inherits(Index, _React$Component2);

    function Index(props) {
      var _this;

      _classCallCheck(this, Index);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(Index).call(this, props));

      _initialize(_assertThisInitialized(_assertThisInitialized(_this)));

      _this.player = react__WEBPACK_IMPORTED_MODULE_1___default.a.createRef();
      return _this;
    } // componentDidMount() {
    //   document.addEventListener("keydown", this.handleKeyPress, false);
    // }
    // componentWillUnmount() {
    //   document.removeEventListener("keydown", this.handleKeyPress, false);
    // }


    return Index;
  }(_React$Component);

  return {
    F: Index,
    d: [{
      kind: "field",
      key: "state",
      value: function value() {
        return {
          source: 'https://download.blender.org/durian/trailer/sintel_trailer-480p.mp4',
          selected: null,
          //{ name: 'New Clip' },
          formErrors: null,
          loading: false,
          play: false,
          edit: false,
          clips: [{
            id: '1',
            name: 'test 1',
            start: '3',
            end: '5'
          }, {
            id: '2',
            name: 'test 2',
            start: '10',
            end: '11'
          }, {
            id: '3',
            name: 'test 1',
            start: '7',
            end: '9'
          }, {
            id: '4',
            name: 'test 2',
            start: '10',
            end: '15'
          }, {
            id: '5',
            name: 'test 1',
            start: '30',
            end: '33'
          }, {
            id: '6',
            name: 'test 2',
            start: '10',
            end: '15'
          }],
          sourceDialog: false,
          resetDialog: false,
          infoDialog: false,
          deleteDialog: false,
          snackbar: null
        };
      }
    }, {
      kind: "field",
      key: "getSrc",
      value: function value() {
        var _this2 = this;

        return function () {
          var _this2$state = _this2.state,
              source = _this2$state.source,
              selected = _this2$state.selected;
          return source ? "".concat(source, "#t=").concat(selected && selected.start || '', ",").concat(selected && selected.end) : '';
        };
      }
    }, {
      kind: "field",
      key: "setSourceDialog",
      value: function value() {
        var _this3 = this;

        return function (sourceDialog) {
          _this3.setState({
            sourceDialog: sourceDialog
          });
        };
      }
    }, {
      kind: "field",
      key: "setSource",
      value: function value() {
        var _this4 = this;

        return function () {
          var sourceDialog = _this4.state.sourceDialog;

          _this4.setState({
            source: sourceDialog,
            sourceDialog: false
          });

          setTimeout(function () {
            var source = _this4.state.source;

            if (source) {
              _this4.setSnackbar('The video was correctly upload.');
            }
          }, 200);
        };
      }
    }, {
      kind: "field",
      key: "setSelected",
      value: function value() {
        var _this5 = this;

        return (
          /*#__PURE__*/
          function () {
            var _ref = _asyncToGenerator(
            /*#__PURE__*/
            _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(selected) {
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (!(selected === 'source' || selected === false)) {
                        _context.next = 4;
                        break;
                      }

                      _this5.setState({
                        selected: false,
                        edit: false
                      });

                      _context.next = 11;
                      break;

                    case 4:
                      if (!(selected === true)) {
                        _context.next = 8;
                        break;
                      }

                      _this5.setState({
                        edit: true,
                        selected: {
                          name: 'New Clip',
                          start: 0,
                          end: false
                        }
                      });

                      _context.next = 11;
                      break;

                    case 8:
                      _this5.setState({
                        loading: true,
                        selected: _objectSpread({}, selected, {
                          start: _this5.secondsToTime(selected.start),
                          end: _this5.secondsToTime(selected.end)
                        })
                      }); // Fake loading (1sec)


                      _context.next = 11;
                      return new Promise(function (resolve) {
                        return setTimeout(function () {
                          _this5.setState({
                            loading: false
                          });

                          resolve();
                        }, 1000);
                      });

                    case 11:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));

            return function (_x) {
              return _ref.apply(this, arguments);
            };
          }()
        );
      }
    }, {
      kind: "field",
      key: "setInfoDialog",
      value: function value() {
        var _this6 = this;

        return function (infoDialog) {
          _this6.setState({
            infoDialog: infoDialog
          });
        };
      }
    }, {
      kind: "field",
      key: "setDeleteDialog",
      value: function value() {
        var _this7 = this;

        return function (deleteDialog) {
          _this7.setState({
            deleteDialog: deleteDialog
          });
        };
      }
    }, {
      kind: "field",
      key: "handleClipChange",
      value: function value() {
        var _this8 = this;

        return function (key, value) {
          var selected = _this8.state.selected;
          selected = _objectSpread({}, selected, _defineProperty({}, key, value));

          _this8.setState({
            selected: selected
          });

          _this8.checkForm(selected);
        };
      }
    }, {
      kind: "field",
      key: "checkForm",
      value: function value() {
        var _this9 = this;

        return function (_ref2) {
          var name = _ref2.name,
              start = _ref2.start,
              end = _ref2.end;
          var errors = {
            name: _this9.nameHasError(name),
            start: _this9.timeHasError(start),
            end: _this9.timeHasError(end)
          };

          if (errors.name || errors.start || errors.end) {
            _this9.setState({
              formErrors: errors
            });

            return false;
          }

          if (start && end && _this9.timeToSeconds(end) - _this9.timeToSeconds(start) <= 0) {
            _this9.setState({
              formErrors: {
                end: 'The end time must be greater than the start time'
              }
            });

            return false;
          }

          _this9.setState({
            formErrors: null
          });

          return true;
        };
      }
    }, {
      kind: "field",
      key: "nameHasError",
      value: function value() {
        return function (name) {
          if (name.length === 0) return 'The name is required';
          if (name.length > 100) return 'The name is to long';
          return false;
        };
      }
    }, {
      kind: "field",
      key: "timeHasError",
      value: function value() {
        return function (time) {
          if (!time) return false;
          if (!isNaN(time) && Number(time) >= 0) return false;
          if (moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'H:m:s', true).isValid()) return false;
          if (moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'm:s', true).isValid()) return false;
          return 'The time format is not valid. Valid format: 00:00:00 or 00:00';
        };
      }
    }, {
      kind: "field",
      key: "setCurrentTime",
      value: function value() {
        var _this10 = this;

        return function (key) {
          _this10.handleClipChange(key, _this10.secondsToTime(_this10.player.time()));
        };
      }
    }, {
      kind: "field",
      key: "timeToSeconds",
      value: function value() {
        return function (time) {
          if (!time) return 0;
          if (!isNaN(time) && Number(time) >= 0) return time;
          if (moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'H:m:s', true).isValid()) return moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'H:m:s').diff(moment__WEBPACK_IMPORTED_MODULE_8___default()().startOf('day'), 'seconds');
          if (moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'm:s', true).isValid()) return moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'm:s').diff(moment__WEBPACK_IMPORTED_MODULE_8___default()().startOf('day'), 'seconds');
        };
      }
    }, {
      kind: "field",
      key: "secondsToTime",
      value: function value() {
        return function (time) {
          return moment__WEBPACK_IMPORTED_MODULE_8___default.a.utc(time * 1000).format('HH:mm:ss');
        };
      }
    }, {
      kind: "field",
      key: "saveClip",
      value: function value() {
        var _this11 = this;

        return function () {
          var _this11$state = _this11.state,
              selected = _this11$state.selected,
              clips = _this11$state.clips;

          if (selected.id) {
            //Edit
            var idx = lodash__WEBPACK_IMPORTED_MODULE_6___default.a.findIndex(clips, {
              'id': selected.id
            });

            clips[idx] = _objectSpread({}, selected, {
              start: _this11.timeToSeconds(selected.start),
              end: _this11.timeToSeconds(selected.end)
            });

            _this11.setState({
              clips: clips,
              selected: false
            });

            _this11.setSnackbar('The clip was successfully edited.');
          } else {
            //Create
            selected = _objectSpread({}, selected, {
              id: uuid_v1__WEBPACK_IMPORTED_MODULE_5___default()(),
              start: _this11.timeToSeconds(selected.start),
              end: _this11.timeToSeconds(selected.end)
            });
            clips = [].concat(_toConsumableArray(clips), [selected]);

            _this11.setState({
              clips: clips,
              selected: false,
              edit: false
            });

            _this11.setSnackbar('The clip was successfully saved.');
          }
        };
      }
    }, {
      kind: "field",
      key: "editClip",
      value: function value() {
        var _this12 = this;

        return function (id) {
          var clips = _this12.state.clips;

          var selected = lodash__WEBPACK_IMPORTED_MODULE_6___default.a.keyBy(clips, 'id')[id];

          _this12.setState({
            edit: true
          });

          _this12.setSelected(selected);
        };
      }
    }, {
      kind: "field",
      key: "onDeleteClip",
      value: function value() {
        var _this13 = this;

        return function (clip) {
          var _this13$state = _this13.state,
              clips = _this13$state.clips,
              selected = _this13$state.selected;
          var idx = clips.indexOf(clip);
          clips.splice(idx, 1);

          if (selected && selected.id === clip.id) {
            _this13.setState({
              clips: clips,
              deleteDialog: false,
              selected: false
            });
          } else {
            _this13.setState({
              clips: clips,
              deleteDialog: false
            });
          }

          _this13.setSnackbar('The clip was successfully deleted.');
        };
      }
    }, {
      kind: "field",
      key: "togglePlay",
      value: function value() {
        var _this14 = this;

        return function () {
          _this14.setState({
            play: !_this14.state.play
          });
        };
      }
    }, {
      kind: "field",
      key: "setPlay",
      value: function value() {
        var _this15 = this;

        return function (play) {
          _this15.setState({
            play: play
          });
        };
      }
    }, {
      kind: "field",
      key: "onPlay",
      value: function value() {
        var _this16 = this;

        return (
          /*#__PURE__*/
          function () {
            var _ref3 = _asyncToGenerator(
            /*#__PURE__*/
            _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(data) {
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.prev = 0;

                      if (!(data !== 'preview')) {
                        _context2.next = 5;
                        break;
                      }

                      _context2.next = 4;
                      return _this16.setSelected(data);

                    case 4:
                      _this16.setState({
                        edit: false
                      });

                    case 5:
                      _this16.player.load();

                      _this16.player.play();

                      _context2.next = 12;
                      break;

                    case 9:
                      _context2.prev = 9;
                      _context2.t0 = _context2["catch"](0);
                      console.log({
                        error: _context2.t0
                      });

                    case 12:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this, [[0, 9]]);
            }));

            return function (_x2) {
              return _ref3.apply(this, arguments);
            };
          }()
        );
      }
    }, {
      kind: "field",
      key: "toggleResetDialog",
      value: function value() {
        var _this17 = this;

        return function () {
          _this17.setState({
            resetDialog: !_this17.state.resetDialog
          });
        };
      }
    }, {
      kind: "field",
      key: "onReset",
      value: function value() {
        var _this18 = this;

        return function (error) {
          _this18.setState({
            source: null,
            selected: null,
            edit: false,
            play: false,
            clips: [],
            resetDialog: false
          });

          if (error !== true) {
            _this18.setSnackbar('The project was successfully reset.');
          }
        };
      }
    }, {
      kind: "field",
      key: "onError",
      value: function value() {
        var _this19 = this;

        return function (e) {
          _this19.setSnackbar('Something went wrong. Try again.');

          _this19.onReset(true);
        };
      }
    }, {
      kind: "field",
      key: "onPause",
      value: function value() {
        var _this20 = this;

        return (
          /*#__PURE__*/
          _asyncToGenerator(
          /*#__PURE__*/
          _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
            var selected, next;
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    selected = _this20.state.selected;

                    _this20.setPlay(false);

                    if (!(_this20.timeToSeconds(selected.end) === Math.round(_this20.player.time()))) {
                      _context3.next = 9;
                      break;
                    }

                    next = _this20.getNextVideo();
                    console.log({
                      next: next
                    });

                    if (!next) {
                      _context3.next = 9;
                      break;
                    }

                    _context3.next = 8;
                    return _this20.setLoadingTimer();

                  case 8:
                    _this20.onPlay(next);

                  case 9:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }))
        );
      }
    }, {
      kind: "field",
      key: "setLoadingTimer",
      value: function value() {
        var _this21 = this;

        return (
          /*#__PURE__*/
          _asyncToGenerator(
          /*#__PURE__*/
          _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return new Promise(function (resolve) {
                      return timer = setTimeout(function () {
                        _this21.setState({
                          loading: '3'
                        });

                        resolve();
                      }, 1000);
                    });

                  case 2:
                    _context4.next = 4;
                    return new Promise(function (resolve) {
                      return timer = setTimeout(function () {
                        _this21.setState({
                          loading: '2'
                        });

                        resolve();
                      }, 1000);
                    });

                  case 4:
                    _context4.next = 6;
                    return new Promise(function (resolve) {
                      return timer = setTimeout(function () {
                        _this21.setState({
                          loading: '1'
                        });

                        resolve();
                      }, 1000);
                    });

                  case 6:
                    _context4.next = 8;
                    return new Promise(function (resolve) {
                      return timer = setTimeout(function () {
                        _this21.setState({
                          loading: false
                        });

                        resolve();
                      }, 1000);
                    });

                  case 8:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }))
        );
      }
    }, {
      kind: "field",
      key: "onStopTimer",
      value: function value() {
        var _this22 = this;

        return function () {
          clearTimeout(timer);

          _this22.setState({
            loading: false
          });
        };
      }
    }, {
      kind: "field",
      key: "setSnackbar",
      value: function value() {
        var _this23 = this;

        return function (snackbar) {
          _this23.setState({
            snackbar: snackbar
          });
        };
      }
    }, {
      kind: "field",
      key: "getNextVideo",
      value: function value() {
        var _this24 = this;

        return function () {
          var _this24$state = _this24.state,
              selected = _this24$state.selected,
              edit = _this24$state.edit,
              clips = _this24$state.clips;
          if (edit) return false;
          if (selected && !selected.id) return false;

          var idx = lodash__WEBPACK_IMPORTED_MODULE_6___default.a.findIndex(clips, {
            'id': selected.id
          });

          if (!clips[idx + 1]) return false;
          return clips[idx + 1];
        };
      }
    }, {
      kind: "field",
      decorators: [Object(react_keydown__WEBPACK_IMPORTED_MODULE_9__["default"])('space')],
      key: "onSpaceKeyPress",
      value: function value() {
        return function () {};
      }
    }, {
      kind: "method",
      key: "render",
      value: function value() {
        var _this25 = this;

        var classes = this.props.classes;
        var _this$state = this.state,
            source = _this$state.source,
            selected = _this$state.selected,
            formErrors = _this$state.formErrors,
            loading = _this$state.loading,
            edit = _this$state.edit,
            clips = _this$state.clips,
            sourceDialog = _this$state.sourceDialog,
            resetDialog = _this$state.resetDialog,
            infoDialog = _this$state.infoDialog,
            deleteDialog = _this$state.deleteDialog,
            snackbar = _this$state.snackbar;
        return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: classes.root,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 474
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default.a, {
          container: true,
          spacing: 24,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 475
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default.a, {
          item: true,
          xs: 12,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 476
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_title__WEBPACK_IMPORTED_MODULE_16__["default"], {
          title: "Video editor",
          subtitle: "By Jorge Mariel",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 477
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default.a, {
          item: true,
          xs: 12,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 479
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_10___default.a, {
          className: classes.paper,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 480
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(Player, {
          src: this.getSrc(),
          start: selected ? selected.start : '10',
          end: selected ? selected.end : false,
          loading: loading,
          setPlay: this.setPlay,
          openSourceDialog: function openSourceDialog() {
            return _this25.setSourceDialog(true);
          },
          onPause: this.onPause,
          onStopTimer: this.onStopTimer,
          onError: this.onError,
          onRef: function onRef(ref) {
            return _this25.player = ref;
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 481
          },
          __self: this
        }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default.a, {
          item: true,
          xs: 12,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 495
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_collapse__WEBPACK_IMPORTED_MODULE_7__["Collapse"], {
          isOpened: !!selected,
          style: {
            overflow: 'inherit'
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 496
          },
          __self: this
        }, !!selected && !!edit && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_clip__WEBPACK_IMPORTED_MODULE_19__["default"], {
          data: selected,
          errors: formErrors,
          handleChange: this.handleClipChange,
          setCurrentTime: this.setCurrentTime,
          onClose: function onClose() {
            return _this25.setSelected(false);
          },
          onPreview: this.onPlay,
          onSave: this.saveClip,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 498
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_collapse__WEBPACK_IMPORTED_MODULE_7__["Collapse"], {
          isOpened: !edit && !!source,
          style: {
            overflow: 'inherit'
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 509
          },
          __self: this
        }, !edit && !!source && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_actions__WEBPACK_IMPORTED_MODULE_18__["default"], {
          onNewClip: function onNewClip() {
            return _this25.setSelected(true);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 511
          },
          __self: this
        }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default.a, {
          item: true,
          xs: 12,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 517
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_collapse__WEBPACK_IMPORTED_MODULE_7__["Collapse"], {
          isOpened: !!source,
          style: {
            overflow: 'inherit'
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 518
          },
          __self: this
        }, !!source && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_10___default.a, {
          className: classes.paper,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 520
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_list__WEBPACK_IMPORTED_MODULE_17__["default"], {
          source: source,
          items: clips,
          selected: selected,
          timeFormat: this.secondsToTime,
          onPlay: this.onPlay,
          onEdit: this.editClip,
          onDelete: this.setDeleteDialog,
          onReset: this.toggleResetDialog,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 521
          },
          __self: this
        }))))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Fab__WEBPACK_IMPORTED_MODULE_12___default.a, {
          onClick: function onClick() {
            return _this25.setInfoDialog(true);
          },
          className: classes.about,
          color: "primary",
          "aria-label": "About",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 536
          },
          __self: this
        }, "i"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_sourceDialog__WEBPACK_IMPORTED_MODULE_20__["default"], {
          value: sourceDialog,
          closeDialog: function closeDialog() {
            return _this25.setSourceDialog(false);
          },
          handleSourceChange: this.setSourceDialog,
          onSubmit: this.setSource,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 547
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_resetDialog__WEBPACK_IMPORTED_MODULE_21__["default"], {
          open: resetDialog,
          onOk: this.onReset,
          onCancel: this.toggleResetDialog,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 553
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_infoDialog__WEBPACK_IMPORTED_MODULE_22__["default"], {
          open: infoDialog,
          onClose: function onClose() {
            return _this25.setInfoDialog(false);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 558
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_deleteDialog__WEBPACK_IMPORTED_MODULE_23__["default"], {
          value: deleteDialog,
          onOk: this.onDeleteClip,
          onCancel: function onCancel() {
            return _this25.setDeleteDialog(false);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 562
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_13___default.a, {
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left'
          },
          open: !!snackbar,
          autoHideDuration: 6000,
          onClose: function onClose() {
            return _this25.setSnackbar(false);
          },
          ContentProps: {
            'aria-describedby': 'message-id'
          },
          message: react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
            id: "message-id",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 580
            },
            __self: this
          }, snackbar),
          action: [react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_14___default.a, {
            key: "close",
            "aria-label": "Close",
            color: "inherit",
            className: classes.close,
            onClick: function onClick() {
              return _this25.setSnackbar(false);
            },
            __source: {
              fileName: _jsxFileName,
              lineNumber: 582
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_15___default.a, {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 589
            },
            __self: this
          }))],
          __source: {
            fileName: _jsxFileName,
            lineNumber: 569
          },
          __self: this
        }));
      }
    }]
  };
}, react__WEBPACK_IMPORTED_MODULE_1___default.a.Component);

Index.propTypes = {
  classes: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__["withStyles"])(styles)(Index));
    (function (Component, route) {
      if(!Component) return
      if (false) {}
      module.hot.accept()
      Component.__route = route

      if (module.hot.status() === 'idle') return

      var components = next.router.components
      for (var r in components) {
        if (!components.hasOwnProperty(r)) continue

        if (components[r].Component.__route === route) {
          next.router.update(r, Component)
        }
      }
    })(typeof __webpack_exports__ !== 'undefined' ? __webpack_exports__.default : (module.exports.default || module.exports), "/")
  
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=index.js.c9b70cf3f70745ae86fe.hot-update.js.map