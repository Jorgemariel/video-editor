webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/next/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/styles/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var uuid_v1__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! uuid/v1 */ "./node_modules/uuid/v1.js");
/* harmony import */ var uuid_v1__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(uuid_v1__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_collapse__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-collapse */ "./node_modules/react-collapse/lib/index.js");
/* harmony import */ var react_collapse__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_collapse__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_keydown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-keydown */ "./node_modules/react-keydown/es/index.js");
/* harmony import */ var _material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/Paper */ "./node_modules/@material-ui/core/Paper/index.js");
/* harmony import */ var _material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/Grid/index.js");
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _material_ui_core_Fab__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/core/Fab */ "./node_modules/@material-ui/core/Fab/index.js");
/* harmony import */ var _material_ui_core_Fab__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Fab__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/core/Snackbar */ "./node_modules/@material-ui/core/Snackbar/index.js");
/* harmony import */ var _material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @material-ui/core/IconButton */ "./node_modules/@material-ui/core/IconButton/index.js");
/* harmony import */ var _material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @material-ui/icons/Close */ "./node_modules/@material-ui/icons/Close.js");
/* harmony import */ var _material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _components_title__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../components/title */ "./components/title.js");
/* harmony import */ var _components_list__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../components/list */ "./components/list.js");
/* harmony import */ var _components_actions__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../components/actions */ "./components/actions.js");
/* harmony import */ var _components_clip__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../components/clip */ "./components/clip.js");
/* harmony import */ var _components_sourceDialog__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../components/sourceDialog */ "./components/sourceDialog.js");
/* harmony import */ var _components_resetDialog__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../components/resetDialog */ "./components/resetDialog.js");
/* harmony import */ var _components_infoDialog__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../components/infoDialog */ "./components/infoDialog.js");
/* harmony import */ var _components_deleteDialog__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../components/deleteDialog */ "./components/deleteDialog.js");
/* harmony import */ var _components_noVideo__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../components/noVideo */ "./components/noVideo.js");


var _jsxFileName = "/home/jorge/Dev/Jobsity/Coding Challenge/video-editor/pages/index.js",
    _dec,
    _class,
    _descriptor,
    _temp;

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _initializerDefineProperty(target, property, descriptor, context) { if (!descriptor) return; Object.defineProperty(target, property, { enumerable: descriptor.enumerable, configurable: descriptor.configurable, writable: descriptor.writable, value: descriptor.initializer ? descriptor.initializer.call(context) : void 0 }); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) { var desc = {}; Object['ke' + 'ys'](descriptor).forEach(function (key) { desc[key] = descriptor[key]; }); desc.enumerable = !!desc.enumerable; desc.configurable = !!desc.configurable; if ('value' in desc || desc.initializer) { desc.writable = true; } desc = decorators.slice().reverse().reduce(function (desc, decorator) { return decorator(target, property, desc) || desc; }, desc); if (context && desc.initializer !== void 0) { desc.value = desc.initializer ? desc.initializer.call(context) : void 0; desc.initializer = undefined; } if (desc.initializer === void 0) { Object['define' + 'Property'](target, property, desc); desc = null; } return desc; }

function _initializerWarningHelper(descriptor, context) { throw new Error('Decorating class property failed. Please ensure that ' + 'proposal-class-properties is enabled and set to use loose mode. ' + 'To use proposal-class-properties in spec mode with decorators, wait for ' + 'the next major version of decorators in stage 2.'); }

// Base








 // Material-UI





 // Icons

 // Custom










var Player = next_dynamic__WEBPACK_IMPORTED_MODULE_4___default()(function () {
  return Promise.all(/*! import() */[__webpack_require__.e("styles"), __webpack_require__.e(1)]).then(__webpack_require__.bind(null, /*! ../components/player */ "./components/player.js"));
}, {
  ssr: false,
  loading: function loading() {
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_noVideo__WEBPACK_IMPORTED_MODULE_24__["default"], {
      loading: true,
      onStopTimer: function onStopTimer() {
        return true;
      },
      openSourceDialog: function openSourceDialog() {
        return true;
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36
      },
      __self: this
    });
  },
  loadableGenerated: {
    webpack: function webpack() {
      return [/*require.resolve*/(/*! ../components/player */ "./components/player.js")];
    },
    modules: ["../components/player"]
  }
});

var styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      paddingTop: theme.spacing.unit * 5,
      maxWidth: 600,
      margin: '0 auto',
      marginBottom: 80,
      padding: '0 15px'
    },
    about: {
      position: 'fixed',
      bottom: theme.spacing.unit * 2,
      right: theme.spacing.unit * 2,
      textTransform: 'none',
      fontSize: '1.5em'
    }
  };
}; // Global var for timeout


var timer;
var Index = (_dec = Object(react_keydown__WEBPACK_IMPORTED_MODULE_9__["default"])('space'), (_class = (_temp =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Index, _React$Component);

  function Index(props) {
    var _this;

    _classCallCheck(this, Index);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Index).call(this, props));
    _this.state = {
      source: 'https://download.blender.org/durian/trailer/sintel_trailer-480p.mp4',
      selected: null,
      //{ name: 'New Clip' },
      formErrors: null,
      loading: false,
      play: false,
      edit: false,
      clips: [{
        id: '1',
        name: 'test 1',
        start: '3',
        end: '5'
      }, {
        id: '2',
        name: 'test 2',
        start: '10',
        end: '11'
      }, {
        id: '3',
        name: 'test 1',
        start: '7',
        end: '9'
      }, {
        id: '4',
        name: 'test 2',
        start: '10',
        end: '15'
      }, {
        id: '5',
        name: 'test 1',
        start: '30',
        end: '33'
      }, {
        id: '6',
        name: 'test 2',
        start: '10',
        end: '15'
      }],
      sourceDialog: false,
      resetDialog: false,
      infoDialog: false,
      deleteDialog: false,
      snackbar: null
    };

    _this.getSrc = function () {
      var _this$state = _this.state,
          source = _this$state.source,
          selected = _this$state.selected;
      return source ? "".concat(source, "#t=").concat(selected && selected.start || '', ",").concat(selected && selected.end) : '';
    };

    _this.setSourceDialog = function (sourceDialog) {
      _this.setState({
        sourceDialog: sourceDialog
      });
    };

    _this.setSource = function () {
      var sourceDialog = _this.state.sourceDialog;

      _this.setState({
        source: sourceDialog,
        sourceDialog: false
      });

      setTimeout(function () {
        var source = _this.state.source;

        if (source) {
          _this.setSnackbar('The video was correctly upload.');
        }
      }, 200);
    };

    _this.setSelected =
    /*#__PURE__*/
    function () {
      var _ref = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(selected) {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(selected === 'source' || selected === false)) {
                  _context.next = 4;
                  break;
                }

                _this.setState({
                  selected: false,
                  edit: false
                });

                _context.next = 11;
                break;

              case 4:
                if (!(selected === true)) {
                  _context.next = 8;
                  break;
                }

                _this.setState({
                  edit: true,
                  selected: {
                    name: 'New Clip',
                    start: 0,
                    end: false
                  }
                });

                _context.next = 11;
                break;

              case 8:
                _this.setState({
                  loading: true,
                  selected: _objectSpread({}, selected, {
                    start: _this.secondsToTime(selected.start),
                    end: _this.secondsToTime(selected.end)
                  })
                }); // Fake loading (1sec)


                _context.next = 11;
                return new Promise(function (resolve) {
                  return setTimeout(function () {
                    _this.setState({
                      loading: false
                    });

                    resolve();
                  }, 1000);
                });

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    _this.setInfoDialog = function (infoDialog) {
      _this.setState({
        infoDialog: infoDialog
      });
    };

    _this.setDeleteDialog = function (deleteDialog) {
      _this.setState({
        deleteDialog: deleteDialog
      });
    };

    _this.handleClipChange = function (key, value) {
      var selected = _this.state.selected;
      selected = _objectSpread({}, selected, _defineProperty({}, key, value));

      _this.setState({
        selected: selected
      });

      _this.checkForm(selected);
    };

    _this.checkForm = function (_ref2) {
      var name = _ref2.name,
          start = _ref2.start,
          end = _ref2.end;
      var errors = {
        name: _this.nameHasError(name),
        start: _this.timeHasError(start),
        end: _this.timeHasError(end)
      };

      if (errors.name || errors.start || errors.end) {
        _this.setState({
          formErrors: errors
        });

        return false;
      }

      if (start && end && _this.timeToSeconds(end) - _this.timeToSeconds(start) <= 0) {
        _this.setState({
          formErrors: {
            end: 'The end time must be greater than the start time'
          }
        });

        return false;
      }

      _this.setState({
        formErrors: null
      });

      return true;
    };

    _this.nameHasError = function (name) {
      if (name.length === 0) return 'The name is required';
      if (name.length > 100) return 'The name is to long';
      return false;
    };

    _this.timeHasError = function (time) {
      if (!time) return false;
      if (!isNaN(time) && Number(time) >= 0) return false;
      if (moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'H:m:s', true).isValid()) return false;
      if (moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'm:s', true).isValid()) return false;
      return 'The time format is not valid. Valid format: 00:00:00 or 00:00';
    };

    _this.setCurrentTime = function (key) {
      _this.handleClipChange(key, _this.secondsToTime(_this.player.time()));
    };

    _this.timeToSeconds = function (time) {
      if (!time) return 0;
      if (!isNaN(time) && Number(time) >= 0) return time;
      if (moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'H:m:s', true).isValid()) return moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'H:m:s').diff(moment__WEBPACK_IMPORTED_MODULE_8___default()().startOf('day'), 'seconds');
      if (moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'm:s', true).isValid()) return moment__WEBPACK_IMPORTED_MODULE_8___default()(time, 'm:s').diff(moment__WEBPACK_IMPORTED_MODULE_8___default()().startOf('day'), 'seconds');
    };

    _this.secondsToTime = function (time) {
      return moment__WEBPACK_IMPORTED_MODULE_8___default.a.utc(time * 1000).format('HH:mm:ss');
    };

    _this.saveClip = function () {
      var _this$state2 = _this.state,
          selected = _this$state2.selected,
          clips = _this$state2.clips;

      if (selected.id) {
        //Edit
        var idx = lodash__WEBPACK_IMPORTED_MODULE_6___default.a.findIndex(clips, {
          'id': selected.id
        });

        clips[idx] = _objectSpread({}, selected, {
          start: _this.timeToSeconds(selected.start),
          end: _this.timeToSeconds(selected.end)
        });

        _this.setState({
          clips: clips,
          selected: false
        });

        _this.setSnackbar('The clip was successfully edited.');
      } else {
        //Create
        selected = _objectSpread({}, selected, {
          id: uuid_v1__WEBPACK_IMPORTED_MODULE_5___default()(),
          start: _this.timeToSeconds(selected.start),
          end: _this.timeToSeconds(selected.end)
        });
        clips = [].concat(_toConsumableArray(clips), [selected]);

        _this.setState({
          clips: clips,
          selected: false,
          edit: false
        });

        _this.setSnackbar('The clip was successfully saved.');
      }
    };

    _this.editClip = function (id) {
      var clips = _this.state.clips;

      var selected = lodash__WEBPACK_IMPORTED_MODULE_6___default.a.keyBy(clips, 'id')[id];

      _this.setState({
        edit: true
      });

      _this.setSelected(selected);
    };

    _this.onDeleteClip = function (clip) {
      var _this$state3 = _this.state,
          clips = _this$state3.clips,
          selected = _this$state3.selected;
      var idx = clips.indexOf(clip);
      clips.splice(idx, 1);

      if (selected && selected.id === clip.id) {
        _this.setState({
          clips: clips,
          deleteDialog: false,
          selected: false
        });
      } else {
        _this.setState({
          clips: clips,
          deleteDialog: false
        });
      }

      _this.setSnackbar('The clip was successfully deleted.');
    };

    _this.togglePlay = function () {
      _this.setState({
        play: !_this.state.play
      });
    };

    _this.setPlay = function (play) {
      _this.setState({
        play: play
      });
    };

    _this.onPlay =
    /*#__PURE__*/
    function () {
      var _ref3 = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(data) {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;

                if (!(data !== 'preview')) {
                  _context2.next = 5;
                  break;
                }

                _context2.next = 4;
                return _this.setSelected(data);

              case 4:
                _this.setState({
                  edit: false
                });

              case 5:
                _this.player.load();

                _this.player.play();

                _context2.next = 12;
                break;

              case 9:
                _context2.prev = 9;
                _context2.t0 = _context2["catch"](0);
                console.log({
                  error: _context2.t0
                });

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 9]]);
      }));

      return function (_x2) {
        return _ref3.apply(this, arguments);
      };
    }();

    _this.toggleResetDialog = function () {
      _this.setState({
        resetDialog: !_this.state.resetDialog
      });
    };

    _this.onReset = function (error) {
      _this.setState({
        source: null,
        selected: null,
        edit: false,
        play: false,
        clips: [],
        resetDialog: false
      });

      if (error !== true) {
        _this.setSnackbar('The project was successfully reset.');
      }
    };

    _this.onError = function (e) {
      _this.setSnackbar('Something went wrong. Try again.');

      _this.onReset(true);
    };

    _this.onPause =
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
      var selected, next;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              selected = _this.state.selected;

              _this.setPlay(false);

              if (!(_this.timeToSeconds(selected.end) === Math.round(_this.player.time()))) {
                _context3.next = 9;
                break;
              }

              next = _this.getNextVideo();
              console.log({
                next: next
              });

              if (!next) {
                _context3.next = 9;
                break;
              }

              _context3.next = 8;
              return _this.setLoadingTimer();

            case 8:
              _this.onPlay(next);

            case 9:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));
    _this.setLoadingTimer =
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              _context4.next = 2;
              return new Promise(function (resolve) {
                return timer = setTimeout(function () {
                  _this.setState({
                    loading: '3'
                  });

                  resolve();
                }, 1000);
              });

            case 2:
              _context4.next = 4;
              return new Promise(function (resolve) {
                return timer = setTimeout(function () {
                  _this.setState({
                    loading: '2'
                  });

                  resolve();
                }, 1000);
              });

            case 4:
              _context4.next = 6;
              return new Promise(function (resolve) {
                return timer = setTimeout(function () {
                  _this.setState({
                    loading: '1'
                  });

                  resolve();
                }, 1000);
              });

            case 6:
              _context4.next = 8;
              return new Promise(function (resolve) {
                return timer = setTimeout(function () {
                  _this.setState({
                    loading: false
                  });

                  resolve();
                }, 1000);
              });

            case 8:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, this);
    }));

    _this.onStopTimer = function () {
      clearTimeout(timer);

      _this.setState({
        loading: false
      });
    };

    _this.setSnackbar = function (snackbar) {
      _this.setState({
        snackbar: snackbar
      });
    };

    _this.getNextVideo = function () {
      var _this$state4 = _this.state,
          selected = _this$state4.selected,
          edit = _this$state4.edit,
          clips = _this$state4.clips;
      if (edit) return false;
      if (selected && !selected.id) return false;

      var idx = lodash__WEBPACK_IMPORTED_MODULE_6___default.a.findIndex(clips, {
        'id': selected.id
      });

      if (!clips[idx + 1]) return false;
      return clips[idx + 1];
    };

    _initializerDefineProperty(_this, "onSpaceKeyPress", _descriptor, _assertThisInitialized(_assertThisInitialized(_this)));

    _this.player = react__WEBPACK_IMPORTED_MODULE_1___default.a.createRef();
    return _this;
  } // componentDidMount() {
  //   document.addEventListener("keydown", this.handleKeyPress, false);
  // }
  // componentWillUnmount() {
  //   document.removeEventListener("keydown", this.handleKeyPress, false);
  // }


  _createClass(Index, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var classes = this.props.classes;
      var _this$state5 = this.state,
          source = _this$state5.source,
          selected = _this$state5.selected,
          formErrors = _this$state5.formErrors,
          loading = _this$state5.loading,
          edit = _this$state5.edit,
          clips = _this$state5.clips,
          sourceDialog = _this$state5.sourceDialog,
          resetDialog = _this$state5.resetDialog,
          infoDialog = _this$state5.infoDialog,
          deleteDialog = _this$state5.deleteDialog,
          snackbar = _this$state5.snackbar;
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: classes.root,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 474
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default.a, {
        container: true,
        spacing: 24,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 475
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default.a, {
        item: true,
        xs: 12,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 476
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_title__WEBPACK_IMPORTED_MODULE_16__["default"], {
        title: "Video editor",
        subtitle: "By Jorge Mariel",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 477
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default.a, {
        item: true,
        xs: 12,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 479
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_10___default.a, {
        className: classes.paper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 480
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(Player, {
        src: this.getSrc(),
        start: selected ? selected.start : '10',
        end: selected ? selected.end : false,
        loading: loading,
        setPlay: this.setPlay,
        openSourceDialog: function openSourceDialog() {
          return _this2.setSourceDialog(true);
        },
        onPause: this.onPause,
        onStopTimer: this.onStopTimer,
        onError: this.onError,
        onRef: function onRef(ref) {
          return _this2.player = ref;
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 481
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default.a, {
        item: true,
        xs: 12,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 495
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_collapse__WEBPACK_IMPORTED_MODULE_7__["Collapse"], {
        isOpened: !!selected,
        style: {
          overflow: 'inherit'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 496
        },
        __self: this
      }, !!selected && !!edit && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_clip__WEBPACK_IMPORTED_MODULE_19__["default"], {
        data: selected,
        errors: formErrors,
        handleChange: this.handleClipChange,
        setCurrentTime: this.setCurrentTime,
        onClose: function onClose() {
          return _this2.setSelected(false);
        },
        onPreview: this.onPlay,
        onSave: this.saveClip,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 498
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_collapse__WEBPACK_IMPORTED_MODULE_7__["Collapse"], {
        isOpened: !edit && !!source,
        style: {
          overflow: 'inherit'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 509
        },
        __self: this
      }, !edit && !!source && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_actions__WEBPACK_IMPORTED_MODULE_18__["default"], {
        onNewClip: function onNewClip() {
          return _this2.setSelected(true);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 511
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_11___default.a, {
        item: true,
        xs: 12,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 517
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_collapse__WEBPACK_IMPORTED_MODULE_7__["Collapse"], {
        isOpened: !!source,
        style: {
          overflow: 'inherit'
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 518
        },
        __self: this
      }, !!source && react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_10___default.a, {
        className: classes.paper,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 520
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_list__WEBPACK_IMPORTED_MODULE_17__["default"], {
        source: source,
        items: clips,
        selected: selected,
        timeFormat: this.secondsToTime,
        onPlay: this.onPlay,
        onEdit: this.editClip,
        onDelete: this.setDeleteDialog,
        onReset: this.toggleResetDialog,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 521
        },
        __self: this
      }))))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Fab__WEBPACK_IMPORTED_MODULE_12___default.a, {
        onClick: function onClick() {
          return _this2.setInfoDialog(true);
        },
        className: classes.about,
        color: "primary",
        "aria-label": "About",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 536
        },
        __self: this
      }, "i"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_sourceDialog__WEBPACK_IMPORTED_MODULE_20__["default"], {
        value: sourceDialog,
        closeDialog: function closeDialog() {
          return _this2.setSourceDialog(false);
        },
        handleSourceChange: this.setSourceDialog,
        onSubmit: this.setSource,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 547
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_resetDialog__WEBPACK_IMPORTED_MODULE_21__["default"], {
        open: resetDialog,
        onOk: this.onReset,
        onCancel: this.toggleResetDialog,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 553
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_infoDialog__WEBPACK_IMPORTED_MODULE_22__["default"], {
        open: infoDialog,
        onClose: function onClose() {
          return _this2.setInfoDialog(false);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 558
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_deleteDialog__WEBPACK_IMPORTED_MODULE_23__["default"], {
        value: deleteDialog,
        onOk: this.onDeleteClip,
        onCancel: function onCancel() {
          return _this2.setDeleteDialog(false);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 562
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_Snackbar__WEBPACK_IMPORTED_MODULE_13___default.a, {
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'left'
        },
        open: !!snackbar,
        autoHideDuration: 6000,
        onClose: function onClose() {
          return _this2.setSnackbar(false);
        },
        ContentProps: {
          'aria-describedby': 'message-id'
        },
        message: react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
          id: "message-id",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 580
          },
          __self: this
        }, snackbar),
        action: [react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_core_IconButton__WEBPACK_IMPORTED_MODULE_14___default.a, {
          key: "close",
          "aria-label": "Close",
          color: "inherit",
          className: classes.close,
          onClick: function onClick() {
            return _this2.setSnackbar(false);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 582
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_material_ui_icons_Close__WEBPACK_IMPORTED_MODULE_15___default.a, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 589
          },
          __self: this
        }))],
        __source: {
          fileName: _jsxFileName,
          lineNumber: 569
        },
        __self: this
      }));
    }
  }]);

  return Index;
}(react__WEBPACK_IMPORTED_MODULE_1___default.a.Component), _temp), (_descriptor = _applyDecoratedDescriptor(_class.prototype, "onSpaceKeyPress", [_dec], {
  configurable: true,
  enumerable: true,
  writable: true,
  initializer: function initializer() {
    return function () {
      console.log('hola');
    };
  }
})), _class));
Index.propTypes = {
  classes: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.object.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_3__["withStyles"])(styles)(Index));
    (function (Component, route) {
      if(!Component) return
      if (false) {}
      module.hot.accept()
      Component.__route = route

      if (module.hot.status() === 'idle') return

      var components = next.router.components
      for (var r in components) {
        if (!components.hasOwnProperty(r)) continue

        if (components[r].Component.__route === route) {
          next.router.update(r, Component)
        }
      }
    })(typeof __webpack_exports__ !== 'undefined' ? __webpack_exports__.default : (module.exports.default || module.exports), "/")
  
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=index.js.c6beb514918c35fb05d5.hot-update.js.map