webpackHotUpdate(1,{

/***/ "./components/player.js":
/*!******************************!*\
  !*** ./components/player.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/styles */ "./node_modules/@material-ui/core/styles/index.js");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_html5video__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-html5video */ "./node_modules/react-html5video/dist/index.js");
/* harmony import */ var react_html5video__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_html5video__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _noVideo__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./noVideo */ "./components/noVideo.js");
/* harmony import */ var react_html5video_dist_styles_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-html5video/dist/styles.css */ "./node_modules/react-html5video/dist/styles.css");
/* harmony import */ var react_html5video_dist_styles_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_html5video_dist_styles_css__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/home/jorge/Dev/Jobsity/Coding Challenge/video-editor/components/player.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





 // CSS


var styles = {
  root: {
    overflow: 'hidden',
    borderRadius: 4,
    height: 320,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'black'
  },
  video: {
    width: '100%',
    height: '100%'
  },
  noVideo: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black'
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  extendedIcon: {
    marginRight: 10
  },
  uploadWrapper: {
    width: '100%',
    margin: 20
  },
  help: {
    color: 'white'
  }
};

var Player =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Player, _React$Component);

  function Player() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Player);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Player)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.load = function () {
      _this.refs.player.videoEl.load();
    };

    _this.play = function () {
      _this.refs.player.videoEl.play();
    };

    _this.pause = function () {
      _this.refs.player.videoEl.pause();
    };

    _this.time = function () {
      return _this.refs.player.videoEl.currentTime;
    };

    _this.duration = function () {
      return _this.refs.player.videoEl.duration;
    };

    _this.error = function () {
      return _this.refs.player.videoEl.error;
    };

    return _this;
  }

  _createClass(Player, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.onRef(this);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.props.onRef(undefined);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          src = _this$props.src,
          loading = _this$props.loading,
          openSourceDialog = _this$props.openSourceDialog,
          setPlay = _this$props.setPlay,
          onPause = _this$props.onPause,
          onStopTimer = _this$props.onStopTimer,
          onError = _this$props.onError,
          classes = _this$props.classes;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: classes.root,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84
        },
        __self: this
      }, !!src && !loading && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_html5video__WEBPACK_IMPORTED_MODULE_3__["DefaultPlayer"], {
        ref: "player",
        className: classes.video,
        controls: ['PlayPause', 'Seek', 'Time', 'Volume', 'Fullscreen'],
        onPlay: function onPlay() {
          return setPlay(true);
        },
        onPause: onPause,
        onError: onError,
        onEnded: function onEnded() {
          return console.log('termino el video');
        },
        height: "100%",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("source", {
        src: src,
        type: "video/webm",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 96
        },
        __self: this
      })), (!src || loading) && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_noVideo__WEBPACK_IMPORTED_MODULE_4__["default"], {
        loading: loading,
        onStopTimer: onStopTimer,
        openSourceDialog: openSourceDialog,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }));
    }
  }]);

  return Player;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

Player.propTypes = {
  classes: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.object.isRequired
};
/* harmony default export */ __webpack_exports__["default"] = (Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__["withStyles"])(styles)(Player));

/***/ })

})
//# sourceMappingURL=1.c6beb514918c35fb05d5.hot-update.js.map