# Video Editor

This project was created to show my knowledge of web technologies and assess my ability to create front​end UI products with attention to software architecture​.

## Try it online

open https://jm-video-editor.herokuapp.com/ on your browser o click [here](https://jm-video-editor.herokuapp.com/)

## How to use

Download the project [or clone the repo](https://gitlab.com/Jorgemariel/video-editor):

```sh
git clone https://gitlab.com/Jorgemariel/video-editor.git
cd video-editor
```

Install it and run:

```sh
npm install
npm run dev
```

sample video link: 
```
https://download.blender.org/durian/trailer/sintel_trailer-480p.mp4
```

## Dependecies

* [React.js](https://reactjs.org/) - A JavaScript library for building user interfaces
* [Next.js](https://github.com/zeit/next.js) - A modern React Framework
* [Material-UI](https://material-ui.com/) - React components that implement Google's Material Design
* [React-HTML5Video](https://github.com/mderrick/react-html5video) - A customizeable HoC for HTML5 Video
* [UUID](https://github.com/kelektiv/node-uuid) - fast generation of RFC4122 UUIDS
* [moment.js](https://momentjs.com/) - Parse, validate, manipulate, and display dates and times
* [lodash](https://lodash.com/) - A modern JavaScript utility library delivering modularity, performance & extras.
* [React-Collapse](https://github.com/nkbt/react-collapse) - For animations
* [React-Keydown](https://github.com/glortho/react-keydown) - A HoC for keydown detection

## Features

* HTML5 Video Player with ​[media fragments](https://www.sitepoint.com/html5-video-fragments-captions-dynamic-thumbnails/).
* Clip list with actions (play, edit, download and delete).
* Clip creator/editor: add/edit clips to list by specifying a name, start time, and end time (with validations).
* The Player, List and other components has the ability to be reuse in other pages without the editing.
* Animations.
* Custom message for information, warnings and errors.
* Autoplay: Automatically jump to the next clip after it finishes, with a 3 second waiting period and appropriate loading animation.
* Save, edit, mount and delete projects in LocalStorage.
* Hotkeys:
  * Enter: Play/Pause
  * Left: Previous video list (if any)
  * Right: Next video list (if any)

