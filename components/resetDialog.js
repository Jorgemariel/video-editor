import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';

const ResetDialog = ({ onCancel, onOk, ...props }) => (
  <Dialog
    disableBackdropClick
    disableEscapeKeyDown
    maxWidth="xs"
    aria-labelledby="confirmation-dialog-title"
    {...props}
  >
    <DialogTitle id="confirmation-dialog-title">Reset All</DialogTitle>
    <DialogContent>
      <Typography>Are you sure you want to erase the project?</Typography>
    </DialogContent>
    <DialogActions>
      <Button onClick={onCancel} color="primary">
        Cancel
      </Button>
      <Button onClick={onOk} color="primary">
        Ok
      </Button>
    </DialogActions>
  </Dialog>
)

export default ResetDialog;