import Typography from '@material-ui/core/Typography';

const Title = ({ title, subtitle, name }) => (
  <div>
    <Typography variant="h4" gutterBottom>
      {title}
    </Typography>
    <Typography variant="subtitle1" gutterBottom>
      {subtitle}
    </Typography>
    <Typography variant="subtitle2" gutterBottom>
      {name}
    </Typography>
  </div>
)

export default Title;