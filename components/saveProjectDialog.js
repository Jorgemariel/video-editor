import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const saveProjectDialog = ({ value, closeDialog, handleNameChange, onSubmit }) => (
  <div>
    <Dialog
      open={value !== false}
      onClose={closeDialog}
      fullWidth={true}
      maxWidth='sm'
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Save Project</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Name"
          type="text"
          fullWidth
          value={value === true || value === false ? '' : value}
          onChange={(e) => handleNameChange(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={closeDialog} color="primary">
          Cancel
        </Button>
        <Button onClick={onSubmit} color="primary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  </div>
);

export default saveProjectDialog;