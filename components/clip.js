import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames'

import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import { Card, CardContent, CardHeader, CardActions } from '@material-ui/core';

// Icons
import UpdateIcon from '@material-ui/icons/Attachment';
import SaveIcon from '@material-ui/icons/Save';

const styles = {
  form: {
    display: 'flex',
  },
  displayFlex: {
    display: 'flex',
    alignItems: 'flex-center',
  },
  name: {
    width: '100%',
  },
  buttonSet: {
    marginLeft: 5,
  },
  time: {
    marginRight: 5,
  },
  leftIcon: {
    marginRight: 5,
  },
  iconSmall: {
    fontSize: 20,
  },
  actions: {
    justifyContent: 'flex-end',
    marginBottom: 10,
    marginRight: 10,
  },
};

const Clip = ({ data, errors, handleChange, setCurrentTime, onClose, onPreview, onSave, classes }) => (
  <Card>
    <CardHeader subheader={data.id ? `Edit '${data.name}'` : 'New Clip'}></CardHeader>
    <CardContent>
      <form className={classes.form} noValidate autoComplete="off">
        <Grid container spacing={16}>
          <Grid item xs={12} className={classes.displayFlex}>
            <TextField
              id="name"
              label="Name"
              value={data.name || ''}
              error={errors && !!errors.name || false}
              onChange={(e) => handleChange('name', e.target.value)}
              className={classes.name}
              helperText={errors && errors.name}
            />
          </Grid>
          <Grid item xs={6} className={classes.displayFlex}>
            <TextField
              id="start"
              label="Start"
              value={data.start || ''}
              error={errors && !!errors.start || false}
              onChange={(e) => handleChange('start', e.target.value)}
              InputProps={{
                endAdornment:
                  <InputAdornment position="end">
                    <Tooltip title="Set current video time as start time" placement="left-start">
                      <IconButton onClick={() => setCurrentTime('start')} ><UpdateIcon /></IconButton>
                    </Tooltip>
                  </InputAdornment>,
              }}
              helperText={errors && errors.start}
              className={classes.time}
            />
          </Grid>
          <Grid item xs={6} className={classes.displayFlex}>
            <TextField
              id="end"
              label="End"
              value={data.end || ''}
              error={errors && !!errors.end || false}
              onChange={(e) => handleChange('end', e.target.value)}
              InputProps={{
                endAdornment:
                  <InputAdornment position="end">
                    <Tooltip title="Set current video time as end time" placement="left-start">
                      <IconButton onClick={() => setCurrentTime('end')} ><UpdateIcon /></IconButton>
                    </Tooltip>
                  </InputAdornment>,
              }}
              helperText={errors && errors.end}
              className={classes.time}
            />
          </Grid>
        </Grid>
      </form>
    </CardContent>
    <CardActions className={classes.actions}>
      <Button size="small" className={classes.button} onClick={onClose}>
        Cancel
      </Button>
      <Button
        size="small"
        className={classes.button}
        onClick={() => onPreview('preview')}
        disabled={(data && !data.start && !data.end) || (errors && (!!errors.start || !!errors.end))}
      >
        Preview
      </Button>
      <Button
        onClick={onSave}
        className={classes.button}
        disabled={(data && !data.start && !data.end) || (errors && (!!errors.name || !!errors.start || !!errors.end))}
        variant="contained"
        color="primary"
        size="small"
      >
        <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
        {data.id ? 'Edit' : 'Save'}
      </Button>
    </CardActions>
  </Card >
)

Clip.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Clip);