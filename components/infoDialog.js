import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';

const InfoDialog = ({ open, onClose }) => (
  <div>
    <Dialog
      open={open}
      onClose={onClose}
      fullWidth={true}
      maxWidth='sm'
    >
      <DialogTitle>Info</DialogTitle>
      <DialogContent>
        <Typography>
          This project was created by Jorge Mariel.
        </Typography>
      </DialogContent>
    </Dialog>
  </div>
);

export default InfoDialog;