import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const SourceDialog = ({ value, closeDialog, handleSourceChange, onSubmit }) => (
  <div>
    <Dialog
      open={value !== false}
      onClose={closeDialog}
      fullWidth={true}
      maxWidth='sm'
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Upload source video</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Paste your video link
        </DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="source"
          label="Video"
          type="text"
          fullWidth
          value={value === true || value === false ? '' : value}
          onChange={(e) => handleSourceChange(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={closeDialog} color="primary">
          Cancel
        </Button>
        <Button onClick={onSubmit} color="primary">
          Upload
        </Button>
      </DialogActions>
    </Dialog>
  </div>
);

export default SourceDialog;