import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MaterialDrawer from '@material-ui/core/Drawer'; //Custom name
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import SaveIcon from '@material-ui/icons/Book';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';

const styles = {
  list: {
    minWidth: 350,
  },
  help: {
    padding: 15,
  }
};

const Drawer = ({ open, projects, onClose, onOpenProject, onDeleteProject, classes }) => (
  <div>
    <MaterialDrawer anchor="right" open={open} onClose={onClose}>
      <div
        tabIndex={0}
        role="button"
        onClick={onClose}
        onKeyDown={onClose}
      >
        <div className={classes.list}>
          <List subheader={<ListSubheader component="div">Projects</ListSubheader>}>
            {!!projects && !!projects.length &&
              projects.map(project => (
                <ListItem button key={project.id} onClick={() => onOpenProject(project)}>
                  <ListItemIcon><SaveIcon /></ListItemIcon>
                  <ListItemText primary={project.name} secondary={project.source} />
                  <ListItemSecondaryAction>
                    <IconButton aria-label="Delete" onClick={() => onDeleteProject(project.id)}>
                      <DeleteIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              ))
            }
            {(!projects || !projects.length) &&
              <Typography className={classes.help}>The project list is empty</Typography>
            }
          </List>
          {/* 
          <Divider />
          <List subheader={<ListSubheader component="div">Clips</ListSubheader>}>
            {clips.map((clip, i) => (
              <ListItem button key={i} onClick={() => onOpenClip(clip)}>
                <ListItemIcon><SaveIcon /></ListItemIcon>
                <ListItemText primary={clip.name} secondary={clip.source} />
              </ListItem>
            ))}
          </List> 
          */}
        </div>
      </div>
    </MaterialDrawer>
  </div>
);

Drawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Drawer);
