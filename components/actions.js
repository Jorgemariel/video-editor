import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';


import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {

};

const Actions = ({ onNewClip, classes }) => (
  <div>
    <Button variant="contained" component="span" className={classes.button} onClick={onNewClip}>
      New Clip
    </Button>
  </div>
)

Actions.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Actions);