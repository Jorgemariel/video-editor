import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Fab from '@material-ui/core/Fab';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';

import UploadIcon from '@material-ui/icons/Backup';
import DownloadIcon from '@material-ui/icons/CloudDownload';

const styles = {
  root: {
    overflow: 'hidden',
    borderRadius: 4,
  },
  video: {
    width: '100%',
    height: '100%',
  },
  noVideo: {
    height: 320,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black'
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  extendedIcon: {
    marginRight: 10,
  },
  uploadWrapper: {
    width: '100%',
    marginTop: 40,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  help: {
    color: 'white',
    margin: 30,
  },
  or: {
    color: 'white',
    margin: 10,
  },
  circularProgress: {
    marginTop: 10,
  },
};

const NoVideo = ({ loading, onStopTimer, openSourceDialog, openDrawer, classes }) => (
  <div className={classes.noVideo}>
    {!loading &&
      <div className={classes.container}>
        <div className={classes.uploadWrapper}>
          <Fab
            variant="extended"
            size="medium"
            color="primary"
            aria-label="Upload"
            className={classes.margin}
            onClick={openSourceDialog}
          >
            <UploadIcon className={classes.extendedIcon} />
            Upload
          </Fab>
          <Typography className={classes.or}>or</Typography>
          <Fab
            variant="extended"
            size="medium"
            color="primary"
            aria-label="Select"
            className={classes.margin}
            onClick={openDrawer}
          >
            <DownloadIcon className={classes.extendedIcon} />
            Select
          </Fab>
        </div>
        <Typography className={classes.help}>Upload or Select a video to start.</Typography>
      </div>
    }
    {!!loading &&
      <div>
        <CircularProgress className={classes.circularProgress} />
        {loading !== true &&
          <div>
            <Typography className={classes.help}>The next video is going to be played in {loading}.</Typography>
            <Fab
              variant="extended"
              size="medium"
              color="primary"
              aria-label="Upload"
              onClick={onStopTimer}
            >
              Cancel
          </Fab>
          </div>
        }
      </div>
    }
  </div>
);

NoVideo.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NoVideo);