import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { DefaultPlayer as Video } from 'react-html5video';

import NoVideo from './noVideo';

// CSS
import 'react-html5video/dist/styles.css';

const styles = {
  root: {
    overflow: 'hidden',
    borderRadius: 4,
    height: 320,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
  video: {
    width: '100%',
    height: '100%',
  },
  noVideo: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black'
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  extendedIcon: {
    marginRight: 10,
  },
  uploadWrapper: {
    width: '100%',
    margin: 20,
  },
  help: {
    color: 'white',
  },
};

class Player extends React.Component {
  componentDidMount() {
    this.props.onRef(this)
  }

  componentWillUnmount() {
    this.props.onRef(undefined)
  }

  load = () => {
    this.refs.player.videoEl.load();
  }

  play = () => {
    this.refs.player.videoEl.play();
  }

  pause = () => {
    this.refs.player.videoEl.pause();
  }

  time = () => {
    return this.refs.player.videoEl.currentTime;
  }

  duration = () => {
    return this.refs.player.videoEl.duration;
  }

  error = () => {
    return this.refs.player.videoEl.error;
  }

  render() {
    const { src, setPlay, onPause, onError, classes } = this.props
    return (
      <div className={classes.root}>
        <Video
          ref='player'
          className={classes.video}
          height='100%'
          controls={['PlayPause', 'Seek', 'Time', 'Volume', 'Fullscreen']}
          onPlay={() => setPlay(true)}
          onPause={onPause}
          onError={onError}
          onEnded={() => console.log('termino el video')}
        >
          <source src={src} type="video/webm" />
        </Video>
      </div>
    );
  }
}

Player.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Player);