import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';

const DeleteDialog = ({ value, onCancel, onOk, ...props }) => (
  <Dialog
    open={value}
    disableBackdropClick
    disableEscapeKeyDown
    maxWidth="xs"
    aria-labelledby="confirmation-dialog-title"
    {...props}
  >
    <DialogTitle id="confirmation-dialog-title">Delete {!!value && value.name}</DialogTitle>
    <DialogContent>
      <Typography>Are you sure you want to delete '{!!value && value.name}'?</Typography>
    </DialogContent>
    <DialogActions>
      <Button onClick={onCancel} color="primary">
        Cancel
      </Button>
      <Button onClick={() => onOk(value)} color="primary">
        Ok
      </Button>
    </DialogActions>
  </Dialog>
)

export default DeleteDialog;