import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import PlayIcon from '@material-ui/icons/PlayArrow';
import DeleteIcon from '@material-ui/icons/Delete';
import DownloadIcon from '@material-ui/icons/Forward';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';

const styles = {
  reset: {
    marginRight: 40,
  },
  download: {
    transform: 'rotate(90deg)',
  },
  secondarySource: {
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    maxWidth: '80%',
  },
}

const VideoList = ({
  source,
  items,
  selected,
  timeFormat,
  onPlay,
  onEdit,
  onDelete,
  onReset,
  classes
}) => (
    <div>
      <List>
        <ListItem
          button
          onClick={() => onPlay('source')}
          selected={source && !!items.length && !selected}
        >
          <ListItemAvatar>
            <Avatar>
              <PlayIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={'Video'}
            secondary={source}
            classes={{ secondary: classes.secondarySource }}
          />
          <ListItemSecondaryAction className={classes.reset}>
            <Button onClick={onReset} variant="contained" color="secondary" size="small">
              Reset
          </Button>
          </ListItemSecondaryAction>
        </ListItem>
        {items.map(item =>
          <ListItem
            key={item.id}
            selected={selected && item.id === selected.id}
            button
            onClick={() => onPlay(item)}
          >
            <ListItemAvatar>
              <Avatar>
                <PlayIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={item.name}
              secondary={`Start: ${timeFormat(item.start)} - End: ${timeFormat(item.end)}`}
            />
            <ListItemSecondaryAction>
              <IconButton aria-label="Edit" onClick={() => onEdit(item.id)}>
                <EditIcon />
              </IconButton>
              {/* 
              <IconButton aria-label="Download">
                <DownloadIcon className={classes.download} />
              </IconButton> 
              */}
              <IconButton aria-label="Delete" onClick={() => onDelete(item)}>
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        )}
      </List>
    </div>
  )

VideoList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(VideoList);